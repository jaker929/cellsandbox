// Jake Rosenfeld 4/10/2017

'use strict';

function Vec2( /* number */ x, /* number */ y) {
	this.x = x;
	this.y = y;
	Object.freeze(this);
}

Vec2.prototype.length = function() {
	return Math.sqrt((this.x * this.x) + (this.y * this.y));
}

Vec2.prototype.plus = function( /* Vec2 */ addend) {
	return new Vec2(this.x + addend.x, this.y + addend.y);
}

Vec2.prototype.minus = function( /* Vec2 */ subtrahend) {
	return new Vec2(this.x - subtrahend.x, this.y - subtrahend.y);
}

Vec2.prototype.timesScalar = function( /* Vec2 */ multiplier) {
	return new Vec2(this.x * multiplier, this.y * multiplier);
}

Vec2.prototype.divideScalar = function( /* Vec2 */ divisor) {
	return this.timesScalar(1 / divisor);
}

Vec2.prototype.normalize = function() {
	return this.divideScalar(this.length());
}

Vec2.prototype.dot = function( /* Vec2 */ other) {
	return ((this.x * other.x) + (this.y * other.y));
}

// (<this> dot normalize(<onto>)) times normalize(<onto>)
Vec2.prototype.project = function( /* Vec2 */ onto) {
	return onto.normalize().timesScalar(this.dot(onto.normalize()));
}

// Ref of <this> about <about> is equal to ((twice the projection of <this> onto <about>) - this)
Vec2.prototype.reflect = function( /* Vec2 */ about) {
	return this.project(about).timesScalar(2).minus(this);
}

// Distance from this cartesian point to another cartesian point
Vec2.prototype.distanceTo = function(/* Vec2 */ other) {
	return other.minus(this).length();
}

Vec2.randomVector = function( /* number */ range) {
	return new Vec2(Math.random() - .5, Math.random() - .5).normalize().timesScalar(Math.random() * range);
}

const MathUtils = {
	
	clamp: function( /* number */ min, /* number */ num, /* number */ max) {
		return (num < min) ? min : ((num > max) ? max : (num));
	},
};