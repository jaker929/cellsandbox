// Jake Rosenfeld 4/8/2017

'use strict';

function Arena(radius) {
	this.radius = radius;
	this.location = new Vec2(0, 0);
}

(function() {

	Arena.prototype.draw = function( /* CanvasGraphics */ canvasGraphics) {
		canvasGraphics.pushFillColor(arenaColor);
		canvasGraphics.drawCircle(this.location, this.radius);
		canvasGraphics.popFillColor();
	}

	Arena.prototype.getRadius = function() {
		return this.radius;
	}

}());
