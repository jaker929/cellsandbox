// Jake Rosenfeld 4/7/2017

'use strict';

(function() {

	const sixtyFPSinMillis = (1/60) * 1000;

	let arenaCanvas;
	let arena;
	let lastLogicalTick;
	let cells = [];

	function init() {
		lastLogicalTick = new Date().getTime();

		arenaCanvas = new CanvasGraphics('arenaCanvas');
		onResize();

		const canvasDimensions = arenaCanvas.getDimensions();
		const smallDim = canvasDimensions.x < canvasDimensions.y ? canvasDimensions.x : canvasDimensions.y;
		const arenaRadius = Math.floor(smallDim / 2) - 5;

		arena = new Arena(arenaRadius);

		const cellRadius = 8;

		for (let i = 0; i < arenaRadius; i++) {
			const loc = Vec2.randomVector(arenaRadius - cellRadius);
			//const loc = Vec2.randomVector(20);
			//const loc = new Vec2(0, 0);
			//const loc = new Vec2(arenaRadius - cellRadius - 1, 0);
			const speed = Vec2.randomVector(100);

			cells.push(new Cell(arena, loc, Math.random() * (cellRadius - 2) + 2, speed));
		}		
	}

	function tick() {
		const time = new Date().getTime();
		const elapsed = time - lastLogicalTick;

		// put in cellmanager
		for (let cell of cells) {
			cell.tick(elapsed);
		}

		lastLogicalTick = time;
	}

	function draw(timestamp) {
		requestAnimationFrame(draw);

		arenaCanvas.colorBackground();
		arena.draw(arenaCanvas);

		// put in cellmanager
		arenaCanvas.pushFillColor(cellColor);
		for (let cell of cells) {
			cell.draw(arenaCanvas);
		}
		arenaCanvas.popFillColor();
	}

	function onResize() {
		if (arenaCanvas) {
			arenaCanvas.updateSizeAndFillScreen();
		}
	}

	document.addEventListener('DOMContentLoaded', function() {
		init();
		window.addEventListener('resize', onResize);
		setInterval(tick, sixtyFPSinMillis);
		requestAnimationFrame(draw);
	});

}());
