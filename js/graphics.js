// Jake Rosenfeld 4/7/2017

'use strict';

//const bgColor = '#051d38';
const bgColor = new Color(5/255, 29/255, 56/255);
//const arenaColor = '#2B4B6F';
const arenaColor = new Color(43/255, 75/255, 111/255);
//const cellColor = '#26705A';
const cellColor = new Color(38/255, 112/255, 90/255);

// Takes RGB combonents as [0, 1] components
function Color( /* number */ r, /* number */ g, /* number */ b) {
	this.r = MathUtils.clamp(0, r, 1);
	this.g = MathUtils.clamp(0, g, 1);
	this.b = MathUtils.clamp(0, b, 1);

	function componentToHex( /* number */ component) {
		let numString = Math.floor(component * 255).toString(16);
		return ("0" + numString).slice(-2);
	}

	this.hex = "#" + componentToHex(this.r) + componentToHex(this.g) + componentToHex(this.b);
	Object.freeze(this);
}

// (function() {
// 	// color functions
// }());

function CanvasGraphics( /* string */ canvasId) {
	this.canvasId = canvasId;
	this.canvas = document.getElementById(canvasId);
	this.ctx = this.canvas.getContext('2d');
	this.dimensions = new Vec2(this.canvas.width, this.canvas.height);
	this.fillStack = [];
	this.pushFillColor(bgColor);
	this.strokeStack = [];
	this.pushStrokeColor(bgColor);
}

(function() {

	CanvasGraphics.prototype.getContext = function() {
		return this.ctx;
	}

	CanvasGraphics.prototype.getDimensions = function() {
		return this.dimensions;
	}

	CanvasGraphics.prototype.setDimensions = function( /* Vec2 */ dimensions) {
		this.dimensions = dimensions;
		this.canvas.width = dimensions.x;
		this.canvas.height = dimensions.y;
	}

	CanvasGraphics.prototype.updateSizeAndFillScreen = function() {
		this.setDimensions(new Vec2(window.innerWidth, window.innerHeight));
	}

	CanvasGraphics.prototype.pushFillColor = function( /* color */ color) {
		this.ctx.fillStyle = color.hex;
		this.fillStack.push(color);
	}

	CanvasGraphics.prototype.popFillColor = function() {
		this.fillStack.pop();
		this.ctx.fillStyle = (this.fillStack[this.fillStack.length - 1]).hex;
	}

	CanvasGraphics.prototype.pushStrokeColor = function( /* color */ color) {
		this.ctx.strokeStyle = color.hex;
		this.strokeStack.push(color);
	}

	CanvasGraphics.prototype.popStrokeColor = function() {
		this.strokeStack.pop();
		this.ctx.strokeStyle = (this.strokeStack[this.strokeStack.length - 1]).hex;
	}

	// Colors the canvas the applied color, or bgColor if no color is supplied
	CanvasGraphics.prototype.colorBackground = function( /* Color */ color = bgColor) {
		this.pushFillColor(color);
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
		this.popFillColor();
	}

	// The cartesian coordiante system is defined with the origin at the center
	// The canvas coordinate system is defined  with the origin at the top right corner
	// Keeping canvas locations around is not useful as the canvas can be resized at any time	
	CanvasGraphics.prototype.toCanvasLocation = function( /* Vec2 */ cartesianLoc)
	{
		return new Vec2(Math.round(cartesianLoc.x + (this.dimensions.x/2)), Math.round((-cartesianLoc.y) + (this.dimensions.y/2)));
	}

	CanvasGraphics.prototype.drawCircle = function( /* Vec2 */ center, /* number */ radius, /* number */ lineWidth = 0) {
		const canvasLocation = this.toCanvasLocation(center);

		this.ctx.beginPath();
      	this.ctx.arc(canvasLocation.x, canvasLocation.y, radius, 0, 2 * Math.PI);
      	this.ctx.fill();
      	if (lineWidth > 0) {
      		this.ctx.lineWidth = lineWidth;
      		this.ctx.stroke();
      	}	
	}

}());
